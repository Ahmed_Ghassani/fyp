const mongoose = require('mongoose');

// Define Schema
const aqmSchema = new mongoose.Schema({
    aqmID: {
        type: Number,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    path: {
        type: String,
        required: true,
    }
})

// Mongoose compiles the model
const Aqm = mongoose.model('Aqm', aqmSchema);

module.exports = Aqm;