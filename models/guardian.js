const mongoose = require('mongoose');

// Define Schema
const guardianSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
    },
    aqmID: {
        type: Number,
        required: true,
    }
})

// Mongoose compiles the model
const Guardian = mongoose.model('Guardian', guardianSchema);

module.exports = Guardian;