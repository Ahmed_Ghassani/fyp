const mongoose = require('mongoose');

// Define Schema
const readingSchema = new mongoose.Schema({
    temperature: {
        type: Number,
        required: true,
        lowercase: true
    },
    humidity: {
        type: Number,
        required: true,
    },
    pm: {
        type: Number,
        required: true,
    },
    carbonDioxide: {
        type: Number,
        required: true,
    },
    aqmID: {
        type: Number,
        required: true,
    }
})

// Mongoose compiles the model
const Reading = mongoose.model('Reading', readingSchema);

module.exports = Reading;