const express = require('express')
const session = require('express-session');
const mongoose = require('mongoose');
const path = require('path');
const axios = require('axios');
const nodemailer = require('nodemailer')
const Guardian = require('./models/guardian')
const Reading = require('./models/reading')
const Aqm = require('./models/aqm')

//Connect to db aqm
mongoose.connect('mongodb://localhost:27017/aqm', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log("Mongo: Connection Open")
    })
    .catch((err) => {
        console.log("Mongo: Connection Failed");
        console.log(err);
    })


const app = express() // Create App object
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
app.use(express.urlencoded({ extended: true }));

app.use(session({secret: 'SecretKey1234'}))

const port = 3000 //Define port number for local host

//Generate Dashboard
app.get('/',async (req, res) => {
    //Check if user is logged in
    if(!req.session.aqmID){
        res.redirect('/login')
    }
    else{
        // Get Latest reading from users air quality monitor
        const ID = req.session.aqmID
        const reading = await Reading.find({aqmID : ID}).sort({ _id: -1 }).limit(1); 
        res.render('dashboard',  ...reading );
    }
})

// Render login page
app.get('/login', async (req, res) => {
    res.render('login');
})

//Render Registration Page
app.get('/register', async (req, res) => {
    res.render('register')
})

//Render Admin page
app.get('/admin', async (req, res) => {
    res.render('admin');
})

//Clears the user session
app.get('/logout', async (req, res) => {
    req.session.destroy(function(err) {
		if(err) {
			console.log(err);
		} else {
			res.redirect('/login');
		}
	});
})

//Handle Login Form
app.post('/login', async (req, res) => {
    let formEmail = req.body.email;
    let formPassword = req.body.password;
    let query = await Guardian.findOne({ email : formEmail, password : formPassword})
    if(query !== null){
        req.session.aqmID = query.aqmID;        
        res.redirect('/');
    }
    else{
        res.redirect('/login');
    }
})

//Handle Registration Form
app.post('/register', async (req, res) => {
    // Extract data from the body of the request
    let formEmail = req.body.email
    let formPassword = req.body.password
    let formAqmID = req.body.aqmID
    let formAqmPassword = req.body.aqmPassword

    //Make sure email doesn't already exist in database
    if(isEmailInUse(formEmail)){
        //Check if aqm credentials matches 
        if(isValidAqmCredentials(formAqmID,formAqmPassword)){
            const newGuardian = new Guardian({email: formEmail, password: formPassword, aqmID: formAqmID})
            newGuardian.save();
            req.session.aqmID = newGuardian.aqmID;        
            res.redirect('/');
        }
        else{
            //invalid Aqm Password and ID
        }
    }
    else{
        res.redirect('/register');
    }
    
})

//Handle Admin Form
app.post('/admin', async (req, res) => {
    let formUsername = req.body.username
    let formPassword = req.body.password
    let formAqmID = req.body.aqmID
    let formAqmPassword = req.body.aqmPassword
    let formAqmPath = req.body.aqmPath
    if(formUsername === "admin123" && formPassword === "admin123"){
        const newAQM = new Aqm({aqmID : formAqmID,password:formAqmPassword,path:formAqmPath})
        console.log(newAQM)
        newAQM.save()
        res.redirect('/admin');
    }
    else{
        res.redirect('/admin');
    }
})




// Run server and let it listen to port 3000
app.listen(port, async () => {
    setInterval(getData, 60000);
    console.log(`Server is listening on port ${port}`)
  })

  // Check if Air quality monitor credentials used during registration is valid
  async function isValidAqmCredentials(aqmID,aqmPassword){
    query = await Aqm.findOne({aqmID:aqmID, password : aqmPassword});
    if(query !== null){
        return true;
    }
    else{
        return false;
    }
}

//Check if account already exists with that email
async function isEmailInUse(email){
    query = await Guardian.findOne({ email : email});
    if(query === null){
        return false;
    }
    else{
        return true;
    }
}

function isCritical(temperature,humidity,pm,co2){
  if(temperature >= 48 || temperature <= 10 ){return true}
  if(humidity > 60 || humidity < 15 ){return true}
  if(pm > 91){return true}
  if(co2 >= 700){return true}
  else{
      return false;
  }
}


let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'aqialertsystem@gmail.com',
        pass: 'Sc19anaa'
      }
  });

async function emailGuardians(aqmID){
    //Get all the guardians registered to the aqm
    const query = Guardian.find({aqmID: aqmID})
    
    //Send email to each of the Guardians
    for(let guardian of query){
        var mailOptions = {
            from: 'aqialertsystem@gmail.com',
            to: guardian.email,
            subject: 'Air quality Monitor levels critical',
            text: `This email is to inform you that the Air quality monitor with ID ${guardian.aqmID} has detected critical readings`
          };

          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
    }
    
} 



// Get latests readings from Arduinos
async function getData(){
    const query = Aqm.distict('path')
    for(let aqm of query){
        axios
            .get(aqm.path)
            .then(res => {
                let temperature = res.body.temperature;
                let humidity = res.body.humidity;
                let pm = res.body.pm;
                let co2 = res.body.co2;
                if(isCritical(temperature,humidity,pm,co2)){
                    emailGuardians(aqm.aqmID)
                }
                const reading = new Reading({temperature,humidity,pm,co2,aqmID:aqm.aqmID})
                reading.save()
            })
            .catch(error => {
                console.error(error);
        });
    }
}